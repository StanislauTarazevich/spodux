import { setTokens, removeTokens } from '../actions';

export default store => next => action => {
  if (action.type === setTokens.toString()) {
    const {
      accessToken,
      refreshToken
    } = action.payload;

    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('refreshToken', refreshToken);
  } else if (action.type === removeTokens.toString()) {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
  }

  return next(action);
};