import { refreshToken } from '../actions/authentication';
import { isLoggedInSelector } from '../selectors/authentication';
import { SPOTIFY_UNAUTHORIZED_ERROR } from "../settings";

export default ({ dispatch, getState }) => next => async action => {
  if (isLoggedInSelector(getState())) {
    try {
      return await next(action);
    } catch (e) {
      if (e.message === SPOTIFY_UNAUTHORIZED_ERROR) {
        await dispatch(refreshToken());
        return next(action);
      } else {
        throw e;
      }
    }
  } else {
    return next(action);
  }
}