import { handleActions, combineActions } from 'redux-actions';
import { setSearchResults, addSearchResults } from '../../actions';

export default handleActions({
  [combineActions(setSearchResults, addSearchResults)](state, action) {
    return action.payload.total;
  }
}, null);