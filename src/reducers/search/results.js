import { handleActions } from 'redux-actions';
import { setSearchResults, addSearchResults } from '../../actions';

export default handleActions({
  [setSearchResults](state, action) {
    return action.payload.items;
  },
  [addSearchResults](state, action) {
    return [...state, ...action.payload.items];
  }
}, []);