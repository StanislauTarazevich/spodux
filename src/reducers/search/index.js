import { combineReducers } from 'redux';
import next from './next';
import query from './query';
import results from './results';
import total from './total-results';

export default combineReducers({
  next,
  query,
  results,
  total
});