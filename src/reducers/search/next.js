import { handleActions, combineActions } from 'redux-actions';
import {addSearchResults, setSearchResults} from '../../actions';

export default handleActions({
  [combineActions(setSearchResults, addSearchResults)](state, action) {
    return action.payload.next;
  }
}, null);