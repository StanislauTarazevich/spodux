import { handleActions } from 'redux-actions';
import { setSearchQuery } from '../../actions';

export default handleActions({
  [setSearchQuery](state, action) {
    return action.payload;
  }
}, '');