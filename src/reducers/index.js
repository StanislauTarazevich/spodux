import { combineReducers } from 'redux';
import authentication from './authentication';
import currentPlayback from './current-playback';
import currentPlaylist from './current-playlist';
import player from './player';
import playlists from './playlists';
import search from './search';

export default combineReducers({
  authentication,
  currentPlayback,
  currentPlaylist,
  player,
  playlists,
  search
});