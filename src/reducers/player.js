import { handleActions } from 'redux-actions';
import { setPlayer, resetPlayer } from '../actions';

export default handleActions({
  [setPlayer](state, action) {
    return action.payload;
  },
  [resetPlayer]() {
    return null;
  }
}, null);