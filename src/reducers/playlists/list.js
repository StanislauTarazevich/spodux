import { handleActions } from 'redux-actions';
import { setPlaylists, addPlaylists } from '../../actions';

export default handleActions({
  [setPlaylists](state, action) {
    return action.payload.items;
  },
  [addPlaylists](state, action) {
    return [...state, ...action.payload.items];
  }
}, []);