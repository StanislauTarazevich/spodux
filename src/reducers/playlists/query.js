import { handleActions } from 'redux-actions';
import { setPlaylistsQuery } from '../../actions';

export default handleActions({
  [setPlaylistsQuery](state, action) {
    return action.payload;
  }
}, '');