import { combineReducers } from 'redux';
import list from './list';
import next from './next';
import query from './query';
import total from './total-results';

export default combineReducers({
  list,
  next,
  query,
  total
});