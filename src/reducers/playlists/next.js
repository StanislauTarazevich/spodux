import { handleActions, combineActions } from 'redux-actions';
import { setPlaylists, addPlaylists } from '../../actions';

export default handleActions({
  [combineActions(setPlaylists, addPlaylists)](state, action) {
    return action.payload.next;
  }
}, null);