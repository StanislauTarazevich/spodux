import { handleActions } from 'redux-actions';
import { setCurrentPlayback } from '../actions';

export default handleActions({
  [setCurrentPlayback](state, action) {
    return action.payload
  }
}, null);