import { handleActions } from 'redux-actions';
import { setTokens, setInitialization, removeTokens } from '../actions';

const accessToken = localStorage.getItem('accessToken');
const refreshToken = localStorage.getItem('refreshToken');

export default handleActions({
  [setTokens](state, action) {
    const { accessToken, refreshToken } = action.payload;
    return {
      ...state,
      accessToken,
      refreshToken,
      isLoggedIn: true
    };
  },
  [setInitialization](state, action) {
    return {
      ...state,
      isInitialized: true
    }
  },
  [removeTokens](state) {
    return {
      ...state,
      accessToken: '',
      refreshToken: '',
      isLoggedIn: false
    }
  }
}, {
  accessToken,
  refreshToken,
  isLoggedIn: false,
  isInitialized: false
});