import { handleActions, combineActions } from 'redux-actions';
import {
  setCurrentPlaylist,
  setCurrentPlaylistTracks,
  addCurrentPlaylistTracks,
  removeCurrentPlaylist
} from '../../actions';

export default handleActions({
  [combineActions(setCurrentPlaylistTracks, addCurrentPlaylistTracks)](state, action) {
    return action.payload.total;
  },
  [combineActions(removeCurrentPlaylist, setCurrentPlaylist)]() {
    return null;
  }
}, null);