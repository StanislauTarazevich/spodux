import { combineReducers } from 'redux';
import id from './id';
import next from './next';
import total from './total';
import tracks from './tracks';

export default combineReducers({
  id,
  next,
  total,
  tracks
});