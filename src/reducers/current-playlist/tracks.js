import { handleActions, combineActions } from 'redux-actions';
import {
  setCurrentPlaylist,
  setCurrentPlaylistTracks,
  addCurrentPlaylistTracks,
  removeCurrentPlaylist
} from '../../actions';

export default handleActions({
  [setCurrentPlaylistTracks](state, action) {
    return action.payload.items;
  },
  [addCurrentPlaylistTracks](state, action) {
    return [...state, ...action.payload.items];
  },
  [combineActions(removeCurrentPlaylist, setCurrentPlaylist)]() {
    return null;
  }
}, null);