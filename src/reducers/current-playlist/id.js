import { handleActions } from 'redux-actions';
import { setCurrentPlaylist, removeCurrentPlaylist } from '../../actions';

export default handleActions({
  [setCurrentPlaylist](state, action) {
    return action.payload;
  },
  [removeCurrentPlaylist]() {
    return null;
  }
}, null);