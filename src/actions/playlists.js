import {
  setPlaylists,
  addPlaylists,
  setPlaylistsQuery,
  setCurrentPlaylist,
  removeCurrentPlaylist,
  setCurrentPlaylistTracks,
  addCurrentPlaylistTracks
} from '../actions';
import {
  getPlaylists as getPlaylistsApi,
  getPlaylistTracks,
  getMorePlaylists as getMorePlaylistsApi
} from '../services/spotify';
import { accessTokenSelector } from '../selectors/authentication';
import { currentPlaylistSelector } from '../selectors/current-playlist';

export const getPlaylists = () => async (dispatch, getState) => {
  const accessToken = accessTokenSelector(getState());
  const playlists = await getPlaylistsApi(accessToken);
  dispatch(setPlaylists(playlists));
};

export const getMorePlaylists = (url) => async (dispatch, getState) => {
  const accessToken = accessTokenSelector(getState());
  const playlists = await getMorePlaylistsApi(url, accessToken);
  dispatch(addPlaylists(playlists));
};

export const searchPlaylists = (query) => (dispatch) => {
  dispatch(setPlaylistsQuery(query));
};

export const selectPlaylist = (id) => async (dispatch, getState) => {
  dispatch(setCurrentPlaylist(id));

  const state = getState();
  const accessToken = accessTokenSelector(state);
  const playlist = currentPlaylistSelector(state);

  const playlistTracks = await getPlaylistTracks(playlist.tracksUrl, accessToken);
  dispatch(setCurrentPlaylistTracks(playlistTracks));
};

export const deselectPlaylist = () => (dispatch) => {
  dispatch(removeCurrentPlaylist());
};

export const getMorePlaylistTracks = (url) => async (dispatch, getState) => {
  const accessToken = accessTokenSelector(getState());
  const playlistTracks = await getPlaylistTracks(url, accessToken);
  dispatch(addCurrentPlaylistTracks(playlistTracks));
};