import {
  setSearchQuery as setSearchQueryAction,
  setSearchResults,
  addSearchResults
} from '../actions';
import { searchTracks, searchMoreTracks } from '../services/spotify';
import { accessTokenSelector } from '../selectors/authentication';

export const submitSearch = (query) => async (dispatch, getState) => {
  dispatch(setSearchQueryAction(query));
  const accessToken = accessTokenSelector(getState());

  const searchResponse = await searchTracks(query, accessToken);
  dispatch(setSearchResults(searchResponse));
};

export const showMoreSearchResults = (url) => async (dispatch, getState) => {
  const accessToken = accessTokenSelector(getState());
  const searchResponse = await searchMoreTracks(url, accessToken);
  dispatch(addSearchResults(searchResponse));
};