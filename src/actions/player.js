import { setPlayer, resetPlayer } from '../actions';

export const listenPreview = (track) => setPlayer(track);
export const dismissPlayer = () => resetPlayer();