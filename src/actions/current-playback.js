import { setCurrentPlayback } from '../actions';
import { getCurrentPlayback as getCurrentPlaybackApi } from '../services/spotify';
import { accessTokenSelector } from '../selectors/authentication';

export const getCurrentPlayback = () => async (dispatch, getState) => {
  const accessToken = accessTokenSelector(getState());
  const currentPlayback = await getCurrentPlaybackApi(accessToken);
  dispatch(setCurrentPlayback(currentPlayback));
};