import { setTokens, setInitialization, removeTokens } from '../actions';
import { me } from '../services/spotify';
import { getTokens, getRefreshedToken } from '../services/server';
import { accessTokenSelector, refreshTokenSelector } from '../selectors/authentication';

export const retrieveTokens = (code) => async (dispatch) => {
  const {
    accessToken,
    refreshToken
  } = await getTokens(code);

  dispatch(setTokens({
    accessToken,
    refreshToken
  }));
};

export const refreshToken = () => async (dispatch, getState) => {
  const refreshToken = refreshTokenSelector(getState());
  if (refreshToken) {
    const accessToken = await getRefreshedToken(refreshToken);
    dispatch(setTokens({
      accessToken,
      refreshToken
    }));
  }
};

export const initialize = () => async (dispatch, getState) => {
  const state = getState();
  const accessToken = accessTokenSelector(state);
  const refreshToken = refreshTokenSelector(state);

  let isLoggedIn = false;

  if (accessToken) {
    try {
      await me(accessToken);
      isLoggedIn = true;
    } catch (e) {
      isLoggedIn = false;
    }

    if (isLoggedIn) {
      dispatch(setTokens({
        accessToken,
        refreshToken
      }));
    }
  }

  if (!isLoggedIn && refreshToken) {
    const accessToken = await getRefreshedToken(refreshToken);
    dispatch(setTokens({
      accessToken,
      refreshToken
    }));
  }

  dispatch(setInitialization());
};

export const logout = () => (dispatch) => {
  dispatch(removeTokens());
};