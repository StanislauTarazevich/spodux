export const accessTokenSelector = state => state.authentication.accessToken;
export const refreshTokenSelector = state => state.authentication.refreshToken;
export const isInitializedSelector = state => state.authentication.isInitialized;
export const isLoggedInSelector = state => state.authentication.isLoggedIn;
