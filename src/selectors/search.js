export const searchResultsSelector = state => state.search.results;
export const searchQuerySelector = state => state.search.query;
export const searchTotalSelector = state => state.search.total;
export const searchNextSelector = state => state.search.next;