export const playerTrackSelector = (state) => state.player;
export const isPlayerVisibleSelector = (state) => Boolean(state.player);