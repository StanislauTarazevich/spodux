import { createSelector } from 'reselect';

export const playlistsSelector = state => state.playlists.list;
export const playlistsTotalSelector = state => state.playlists.total;
export const playlistsNextSelector = state => state.playlists.next;
export const playlistsQuerySelector = state => state.playlists.query;

export const filteredPlaylistsSelector = createSelector(
  playlistsSelector, playlistsQuerySelector,
  (playlists, query) => playlists.filter((playlist) => playlist.name.toLowerCase().includes(query.toLowerCase()))
);