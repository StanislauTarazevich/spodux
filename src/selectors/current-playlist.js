import { createSelector } from 'reselect';
import { playlistsSelector } from './playlists'

const currentPlaylistIdSelector = (state) => state.currentPlaylist.id;
export const currentPlaylistTracksSelector = (state) => state.currentPlaylist.tracks;
export const currentPlaylistTotalTracksSelector = (state) => state.currentPlaylist.total;
export const currentPlaylistNextTracksSelector = (state) => state.currentPlaylist.next;

export const currentPlaylistSelector = createSelector(
  playlistsSelector, currentPlaylistIdSelector,
  (playlists, id) => playlists.find((playlist) => playlist.id === id)
);