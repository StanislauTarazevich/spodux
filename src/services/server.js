import qs from 'qs';

export const getTokens = async (code) => {
  const requestData = {
    code: code,
    redirect_uri: `${window.location.protocol}//${window.location.host}/login/complete`
  };
  const requestQueryString = qs.stringify(requestData);

  const response = await fetch(`http://localhost:3001/api/token?${requestQueryString}`);
  const responseJson = await response.json();

  const accessToken = responseJson.access_token;
  const refreshToken = responseJson.refresh_token;

  return {
    accessToken,
    refreshToken
  };
};

export const getRefreshedToken = async (refreshToken) => {
  const requestData = {
    refresh_token: refreshToken
  };
  const requestQueryString = qs.stringify(requestData);

  const response = await fetch(`http://localhost:3001/api/token/refresh?${requestQueryString}`);
  const responseJson = await response.json();

  const accessToken = responseJson.access_token;

  return accessToken;
};