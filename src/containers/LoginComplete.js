import React, { PureComponent } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import qs from 'qs';
import { retrieveTokens } from '../actions/authentication';
import { isLoggedInSelector } from '../selectors/authentication';

class LogInComplete extends PureComponent {
  constructor(props) {
    super(props);
    const queryObject = qs.parse(this.props.location.search.substr(1));
    this.state = {
      code: queryObject.code
    };
  }

  componentWillMount() {
    const code = this.state.code;
    if (code) {
      this.props.retrieveTokens(code);
    }
  }

  render() {
    return (
      this.props.isLoggedIn
      ? <Redirect to="/" />
      : this.state.code
        ? <div className="text-center">Logging in</div>
        : <div className="text-center">Login failed</div>
    );
  }
}

const mapStateToProps = (state) => ({
  isLoggedIn: isLoggedInSelector(state)
});

const mapDispatchToProps = {
  retrieveTokens
};

export default connect(mapStateToProps, mapDispatchToProps)(LogInComplete)