import React, { PureComponent } from 'react';
import { BrowserRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import classNames from 'classnames';
import Menu from '../components/Menu';
import Content from '../components/Content';
import PlayerContainer from './Player';
import Loading from '../components/Loading';
import { initialize } from '../actions/authentication';
import { isInitializedSelector, isLoggedInSelector } from '../selectors/authentication';
import { isPlayerVisibleSelector } from '../selectors/player';
import {searchQuerySelector} from '../selectors/search';

class App extends PureComponent {
  componentWillMount() {
    this.props.initialize();
  }

  render() {
    const {
      isInitialized,
      isLoggedIn,
      isPlayerVisible,
      query
    } = this.props;

    const appClass = classNames({
      'app': true,
      'with-player': isPlayerVisible
    });

    return (
      <BrowserRouter>
        {isInitialized
          ? <div className={appClass}>
              <Content isLoggedIn={isLoggedIn} />
              <Menu isLoggedIn={isLoggedIn} query={query} />
              <PlayerContainer/>
            </div>
          : <div className={appClass}>
              <Loading/>
            </div>
        }
      </BrowserRouter>
    );
  }
}

const mapStateToPros = (state) => ({
  isInitialized: isInitializedSelector(state),
  isLoggedIn: isLoggedInSelector(state),
  isPlayerVisible: isPlayerVisibleSelector(state),
  query: searchQuerySelector(state)
});

const mapDispatchToProps = {
  initialize
};

export default connect(mapStateToPros, mapDispatchToProps)(App);
