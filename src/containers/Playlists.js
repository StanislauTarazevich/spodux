import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PlaylistsPage from '../components/playlists/Page';
import {
  getPlaylists,
  getMorePlaylists,
  searchPlaylists,
  selectPlaylist,
  deselectPlaylist,
  getMorePlaylistTracks
} from '../actions/playlists';
import { listenPreview } from '../actions/player';
import {
  filteredPlaylistsSelector,
  playlistsQuerySelector,
  playlistsTotalSelector,
  playlistsNextSelector
} from '../selectors/playlists';
import {
  currentPlaylistSelector,
  currentPlaylistNextTracksSelector,
  currentPlaylistTotalTracksSelector,
  currentPlaylistTracksSelector
} from '../selectors/current-playlist';

class PlaylistsContainer extends PureComponent {
  componentWillMount() {
    this.props.getPlaylists();
  }

  render() {
    const {
      playlists,
      total,
      next,
      query,
      currentPlaylist,
      currentPlaylistTracks,
      currentPlaylistTracksTotal,
      currentPlaylistTracksNext,
      getMorePlaylists,
      searchPlaylists,
      selectPlaylist,
      deselectPlaylist,
      getMorePlaylistTracks,
      listenPreview
    } = this.props;

    return (
      <PlaylistsPage
        playlists={playlists}
        total={total}
        next={next}
        query={query}
        currentPlaylist={currentPlaylist}
        currentPlaylistTracks={currentPlaylistTracks}
        currentPlaylistTracksTotal={currentPlaylistTracksTotal}
        currentPlaylistTracksNext={currentPlaylistTracksNext}
        getMorePlaylists={getMorePlaylists}
        getMorePlaylistTracks={getMorePlaylistTracks}
        onSearch={searchPlaylists}
        onSelectPlaylist={selectPlaylist}
        onModalClose={deselectPlaylist}
        onListenPreview={listenPreview}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    playlists: filteredPlaylistsSelector(state),
    query: playlistsQuerySelector(state),
    total: playlistsTotalSelector(state),
    next: playlistsNextSelector(state),

    currentPlaylist: currentPlaylistSelector(state),
    currentPlaylistTracks: currentPlaylistTracksSelector(state),
    currentPlaylistTracksTotal: currentPlaylistTotalTracksSelector(state),
    currentPlaylistTracksNext: currentPlaylistNextTracksSelector(state)
  };
};

const mapDispatchToProps = {
  getPlaylists,
  getMorePlaylists,
  searchPlaylists,
  selectPlaylist,
  deselectPlaylist,
  getMorePlaylistTracks,
  listenPreview
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistsContainer);