import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import qs from 'qs';
import { submitSearch, showMoreSearchResults } from '../actions/search';
import SearchPage from '../components/search/Page';
import {
  searchNextSelector,
  searchQuerySelector,
  searchResultsSelector,
  searchTotalSelector
} from '../selectors/search';
import {listenPreview} from '../actions/player';

class SearchContainer extends PureComponent {
  componentWillMount() {
    const query = this.getQueryFromSearch(this.props.location.search);
    if (this.props.query !== query) {
      this.setQuery(this.props);
    }
  }

  componentWillUpdate(nextProps) {
    const query = this.getQueryFromSearch(this.props.location.search);
    const nextQuery = this.getQueryFromSearch(nextProps.location.search);
    if (nextQuery !== query) {
      this.setQuery(nextProps);
    }
  }

  getQueryFromSearch(search) {
    const queryString = search.substr(1);
    const queryObject = qs.parse(queryString);
    return queryObject.query;
  }

  setQuery(props) {
    const queryString = props.location.search.substr(1);
    const queryObject = qs.parse(queryString);
    if (queryObject.query) {
      props.submitSearch(queryObject.query);
    }
  }

  handleSubmit = (query) => {
    if (query) {
      this.props.history.push(`/search?query=${query}`);
    }
  };

  render() {
    return (
      <SearchPage
        onSearchSubmit={this.handleSubmit}
        onShowMore={this.props.showMoreSearchResults}
        onListenPreview={this.props.listenPreview}
        next={this.props.next}
        query={this.props.query}
        results={this.props.results}
        total={this.props.total}
        />
    );
  }
}

const mapStateToProps = (state) => ({
  query: searchQuerySelector(state),
  results: searchResultsSelector(state),
  total: searchTotalSelector(state),
  next: searchNextSelector(state)
});

const mapDispatchToProps = {
  submitSearch,
  showMoreSearchResults,
  listenPreview
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchContainer);