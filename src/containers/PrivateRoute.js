import React, { PureComponent } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { isLoggedInSelector } from '../selectors/authentication';

class PrivateRoute extends PureComponent {
  render() {
    const { isLoggedIn, ...props } = this.props;
    if (isLoggedIn) {
      return <Route {...props} />
    } else {
      return <Redirect to="/login" />
    }
  }
}

const mapStateToProps = (state) => ({
  isLoggedIn: isLoggedInSelector(state)
});

export default connect(mapStateToProps, {})(PrivateRoute);