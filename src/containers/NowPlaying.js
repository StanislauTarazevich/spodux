import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import NowPlayingPage from '../components/now-playing/Page';
import { getCurrentPlayback } from '../actions/current-playback';
import { listenPreview } from '../actions/player';
import { currentPlaybackSelector } from '../selectors/current-playback';

class NowPlayingContainer extends PureComponent {
  componentWillMount() {
    this.props.getCurrentPlayback();
  }

  handleRefresh = () => {
    this.props.getCurrentPlayback();
  };

  render() {
    const {
      currentPlayback,
      listenPreview
    } = this.props;

    return (
      <NowPlayingPage
        track={currentPlayback}
        onListenPreview={listenPreview}
        onRefresh={this.handleRefresh}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  currentPlayback: currentPlaybackSelector(state)
});

const mapDispatchToProps = {
  getCurrentPlayback,
  listenPreview
};

export default connect(mapStateToProps, mapDispatchToProps)(NowPlayingContainer);