import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { playerTrackSelector } from '../selectors/player';
import PlayerWidget from '../components/Player';
import { dismissPlayer } from '../actions/player';

class PlayerContainer extends PureComponent {
  render() {
    const {
      track,
      dismissPlayer
    } = this.props;

    return (
      <PlayerWidget
        track={track}
        onClose={dismissPlayer}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  track: playerTrackSelector(state)
});

const mapDispatchToProps = {
  dismissPlayer
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayerContainer);