import React, { PureComponent } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../actions/authentication';
import { isLoggedInSelector } from '../selectors/authentication';

class LogOut extends PureComponent {
  componentWillMount() {
    this.props.logout();
  }

  render() {
    if (this.props.isLoggedIn) {
      return <div>Logging out...</div>
    } else {
      return <Redirect to="/" />
    }
  }
}

const mapStateToProps = (state) => ({
  isLoggedIn: isLoggedInSelector(state)
});

const mapDispatchToProps = {
  logout
};

export default connect(mapStateToProps, mapDispatchToProps)(LogOut);