// Polyfills
import 'babel-polyfill';
import 'whatwg-fetch';
// ---------

import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import ReactModal from 'react-modal';
import reducer from './reducers';
import thunk from 'redux-thunk';
import App from './containers/App';
import tokenMiddleware from './middlewares/local-storage-tokens';
import spotifyAuthMiddleware from './middlewares/spotify-auth-error';
import registerServiceWorker from './registerServiceWorker';

ReactModal.setAppElement('#root');

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(applyMiddleware(
  spotifyAuthMiddleware,
  thunk,
  tokenMiddleware
)));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  ,
  document.getElementById('root')
);
registerServiceWorker();
