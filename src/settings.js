import qs from 'qs';

const spotifyLoginObject = {
  client_id: '7be6613af1654fcd8dbb0a7a8b5c6c93',
  response_type: 'code',
  redirect_uri: `${window.location.protocol}//${window.location.host}/login/complete`,
  scope: 'streaming user-read-playback-state user-modify-playback-state user-read-currently-playing playlist-read-private playlist-read-collaborative  user-read-private'
};

const spotifyLoginQueryString = qs.stringify(spotifyLoginObject);

export const SPOTIFY_LOGIN_URL = `https://accounts.spotify.com/authorize?${spotifyLoginQueryString}`;
export const SPOTIFY_UNAUTHORIZED_ERROR = 'SPOTIFY_UNAUTHORIZED_ERROR';