import React, { PureComponent } from 'react';
import _ from 'lodash';
import classNames from 'classnames';

class PlayerWidget extends PureComponent {
  state = {
    isPlaying: true,
    isExpanded: true,
    progress: 0
  };

  setAudioRef = (ref) => {
    this.audioEl = ref;
  };

  handleEnded = () => {
    this.setState(() => ({
      isPlaying: false
    }));
  };

  handleTimeUpdate = _.throttle(() => {
    if (this.audioEl) {
      const duration = this.audioEl.duration;
      const currentTime = this.audioEl.currentTime;
      const width = (currentTime / duration) * 100;
      this.setState(() => ({
        width
      }));
    }
  }, 500);

  handlePause = () => {
    this.setState((prevState) => ({
      isPlaying: !prevState.isPlaying
    }), () => {
      if (this.state.isPlaying) {
        this.audioEl.play();
      } else {
        this.audioEl.pause();
      }
    });
  };

  handleExpand = () => {
    this.setState((prevState) => ({
      isExpanded: !prevState.isExpanded
    }));
  };

  handleClose = () => {
    this.props.onClose();
  };

  componentWillReceiveProps() {
    this.setState(() => ({
      isPlaying: true,
      progress: 0
    }));
  }

  render() {
    const { track } = this.props;
    const {
      isPlaying,
      isExpanded
    } = this.state;

    const imageClass = classNames({
      'player__image-container hidden-xs': true,
      'hidden': !isExpanded
    });

    if (track) {
      return (
        <div className="player__window">
          <div className={imageClass}>
            <img src={track.albumImage} alt={track.name} className="player__image full-width" />
          </div>
          <div className="player__progress">
            <div className="player__progress-active" style={{width: `${this.state.width}%`}} />
          </div>
          <div className="player__controls-container">
            <div className="player__control">
              <i className="material-icons" onClick={this.handlePause}>
                {isPlaying ? 'pause' : 'play_arrow'}
              </i>
            </div>
            <div className="player__summary">
              <div className="player__track-text">{track.name}</div>
              <small className="player__track-text">{track.artistName}</small>
            </div>
            <div className="player__visibility">
              <span className="player__visibility-control hidden-xs" onClick={this.handleExpand}>
                <i className="material-icons">
                  {isExpanded ? 'expand_more' : 'expand_less'}
                </i>
              </span>
              <span className="player__visibility-control" onClick={this.handleClose}>
                <i className="material-icons">close</i>
              </span>
            </div>
          </div>
          <audio
            src={track.preview}
            autoPlay={true}
            ref={this.setAudioRef}
            onEnded={this.handleEnded}
            onTimeUpdate={this.handleTimeUpdate}
          />
        </div>
      );
    } else {
      return '';
    }
  }
}

export default PlayerWidget;