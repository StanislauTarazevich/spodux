import React, { PureComponent } from 'react';
import SearchQueryForm from './QueryForm';
import SearchResultsList from './ResultsList';

class SearchPage extends PureComponent {
  handleShowMore = () => {
    this.props.onShowMore(this.props.next);
  };

  render() {
    const {
      results,
      total,
      query,
      next,
      onSearchSubmit,
      onListenPreview
    } = this.props;

    let totalContent;
    if (total) {
      totalContent = <h3>{total} tracks found</h3>
    } else if (total === 0) {
      totalContent = <h3>No tracks found :(</h3>
    } else {
      totalContent = <h2>Search for your favorite music</h2>
    }

    return (
      <div>
        <SearchQueryForm
          onSearchSubmit={onSearchSubmit}
          query={query}
        />
        <div className="search__total">{totalContent}</div>
        <SearchResultsList
          results={results}
          onListenPreview={onListenPreview}
        />
        {next && <div className="search__next text-center"><button className="button" onClick={this.handleShowMore}>Show more</button></div>}
      </div>
    );
  }
}

export default SearchPage;