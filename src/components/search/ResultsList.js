import React, { PureComponent } from 'react';
import SearchResult from './Result';

class SearchResultsList extends PureComponent {
  render() {
    const {
      results,
      onListenPreview
    } = this.props;

    return (
      <div className="search__results-list">
        {results.map((track) => (
          <SearchResult
            key={track.id}
            track={track}
            onListenPreview={onListenPreview}
          />
        ))}
      </div>
    );
  }
}

export default SearchResultsList;