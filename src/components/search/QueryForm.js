import React, { PureComponent } from 'react';

class SearchQueryForm extends PureComponent {
  state = {
    query: ''
  };

  componentWillMount() {
    this.setQueryFromProps(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.setQueryFromProps(nextProps);
  }

  setQueryFromProps(props) {
    this.setState(() => ({
      query: props.query
    }));
  }

  handleInput = (event) => {
    const query = event.target.value;
    this.setState(() => ({
      query
    }));
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const query = this.state.query;
    if (query.length > 0) {
      this.props.onSearchSubmit(query);
    }
  };

  render() {
    const {
      query
    } = this.state;

    return (
      <form
        onSubmit={this.handleSubmit}
        className="search__form"
      >
        <input
          onChange={this.handleInput}
          value={query}
          className="search__form-input"
          placeholder="Search..."
        />
        <button
          type="submit"
          className="search__form-submit"
        >
          <i className="material-icons">search</i>
        </button>
      </form>
    );
  }
}

export default SearchQueryForm;