import React, { PureComponent } from 'react';
import classNames from 'classnames';

class SearchResult extends PureComponent {
  hasPreview() {
    const track = this.props.track;
    return Boolean(track && track.preview);
  }

  handleListenPreview = () => {
    if (this.hasPreview()) {
      this.props.onListenPreview(this.props.track);
    }
  };

  render() {
    const track = this.props.track;

    const previewClass = classNames({
      'search__result-control': true,
      'search__result-control--disabled': !this.hasPreview()
    });

    return (
      <div className="search__result">
        <div
          className="search__result-inner"
          style={{backgroundImage: `url(${track.albumImage})`}}
        >
          <div className="search__result-controls">
            <span className={previewClass} onClick={this.handleListenPreview}>
              <i className="material-icons">audiotrack</i>
            </span>
          </div>
          <div className="search__result-summary">
            <div
              className="search__result-name"
              title={track.name}
            >
              {track.name}
            </div>
            <div
              className="search__result-artist-name"
              title={track.artistName}
            >
              <small>{track.artistName}</small>
            </div>
            <div
              className="search__result-album-name"
              title={track.albumName}
            >
              <small>{track.albumName}</small>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default SearchResult;