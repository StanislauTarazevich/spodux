import React, { PureComponent } from 'react';
import { SPOTIFY_LOGIN_URL } from '../settings';

class LogIn extends PureComponent {
  render() {
    return (
      <div className="text-center">
        <h1>Please log in with your Spotify account</h1>
        <a className="button button-large login__button" href={SPOTIFY_LOGIN_URL}>Log In</a>
      </div>
    );
  }
}

export default LogIn;