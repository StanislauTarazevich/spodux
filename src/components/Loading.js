import React, { PureComponent } from 'react';

class Loading extends PureComponent {
  render() {
    return (
      <div className="loader">Loading...</div>
    );
  }
}

export default Loading;