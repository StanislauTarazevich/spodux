import React, { PureComponent } from 'react';

class NotFound extends PureComponent {
  render() {
    return <div>Not Found :(</div>;
  }
}

export default NotFound;