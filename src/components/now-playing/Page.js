import React, { PureComponent } from 'react';
import PageTitle from '../PageTitle';
import NowPlayingControl from './Control';

class NowPlayingPage extends PureComponent {
  render() {
    const {
      track,
      onRefresh,
      onListenPreview
    } = this.props;

    return (
      <div>
        <PageTitle>
          Now Playing
        </PageTitle>
        <NowPlayingControl
          track={track}
          onRefresh={onRefresh}
          onListenPreview={onListenPreview}
        />
      </div>
    );
  }
}

export default NowPlayingPage;