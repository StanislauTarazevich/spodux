import React, { PureComponent } from 'react';

class NowPlayingCurrent extends PureComponent {
  render() {
    const track = this.props.track;

    return (
      <div>
        <div className="now-playing__image-container">
          <img
            src={track.albumImage}
            alt={track.name}
            className="full-width"
          />
        </div>
        <div className="now-playing__summary">
          <div
            className="now-playing__name"
            title={track.name}
          >
            {track.name}
          </div>
          <div
            className="now-playing__artist-name"
            title={track.artistName}
          >
            <small>{track.artistName}</small>
          </div>
          <div
            className="now-playing__album-name"
            title={track.albumName}
          >
            <small>{track.albumName}</small>
          </div>
        </div>
      </div>
    );
  }
}

export default NowPlayingCurrent;