import React, { PureComponent } from 'react';
import classNames from 'classnames';
import NowPlayingCurrent from './Current';

class NowPlayingPage extends PureComponent {
  handleRefresh = () => {
    this.props.onRefresh();
  };

  handleListenPreview = () => {
    if (this.hasPreview()) {
      this.props.onListenPreview(this.props.track);
    }
  };

  hasPreview() {
    const track = this.props.track;
    return track && track.preview
  }

  render() {
    const {
      track
    } = this.props;

    const previewClass = classNames({
      'now-playing__playback-control': true,
      'now-playing__playback-control--disabled': !this.hasPreview()
    });

    return (
      <div className="now-playing__control">
        <div className="now-playing__control-inner" >
          <div>
            <span className={previewClass} onClick={this.handleListenPreview}>
              <i className="material-icons">audiotrack</i>
            </span>
            <span className="now-playing__playback-control" onClick={this.handleRefresh}>
              <i className="material-icons">replay</i>
            </span>
          </div>
          {this.props.track
            ? <NowPlayingCurrent track={track}/>
            : <div>There are no tracks playing</div>
          }
        </div>
      </div>
    );
  }
}

export default NowPlayingPage;