import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { SPOTIFY_LOGIN_URL } from '../settings';

const searchPathname = '/search';

class Menu extends Component {
  isSearchLinkActive = (match, location) => location.pathname === searchPathname;

  render() {
    const {
      query,
      isLoggedIn
    } = this.props;

    let searchUrl = searchPathname;
    if (query) {
      searchUrl += `?query=${query}`;
    }

    return (
      <aside className='menu'>
        <div className='menu__logo'>
          <img src='/icon.png' className="full-width" alt="Spodux logo" />
          <br/>
          <h3 className="text-center">Spodux</h3>
        </div>
        {isLoggedIn
          ? <nav className='menu__nav text-center'>
              <NavLink
                className="menu__link"
                activeClassName="menu__link--active"
                to={searchUrl}
                isActive={this.isSearchLinkActive}
              >
                <i className="material-icons">search</i>
                <br/>
                Search
              </NavLink>
              <NavLink exact className="menu__link" activeClassName="menu__link--active" to='/'>
                <i className="material-icons">play_circle_filled</i>
                <br/>
                Now Playing
              </NavLink>
              <NavLink exact className="menu__link" activeClassName="menu__link--active" to='/playlists'>
                <i className="material-icons">queue_music</i>
                <br/>
                My Playlists
              </NavLink>
            </nav>
          : <nav className='menu__nav text-center'>
              <a className="menu__link" href={SPOTIFY_LOGIN_URL}>
                <i className="material-icons">exit_to_app</i>
                <br/>
                Log In
              </a>
            </nav>
        }

      </aside>
    );
  }
}

export default Menu;