import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PrivateRoute from '../containers/PrivateRoute';
import NowPlayingContainer from '../containers/NowPlaying';
import SearchContainer from '../containers/Search';
import PlaylistsContainer from '../containers/Playlists';
import LogIn from '../components/LogIn';
import LogInComplete from '../containers/LoginComplete';
import LogOut from '../containers/LogOut';
import NotFound from '../components/NotFound';

class Content extends Component {
  render() {
    return (
      <section className='content'>
        <Switch>
          <Route exact path="/login" component={LogIn} />
          <Route exact path="/login/complete" component={LogInComplete} />
          <PrivateRoute exact path="/" component={NowPlayingContainer}/>
          <PrivateRoute exact path="/search" component={SearchContainer} />
          <PrivateRoute exact path="/playlists" component={PlaylistsContainer} />
          <PrivateRoute exact path="/logout" component={LogOut} />
          <Route component={NotFound} />
        </Switch>
      </section>
    );
  }
}

export default Content;