import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

class PageTitle extends PureComponent {
  render() {
    return (
      <div className="clearfix">
        <h1 className="pull-left">
          {this.props.children}
        </h1>
        <div className="pull-right">
          <a
            className="button button-icon"
            title="Spotify account"
            href="https://www.spotify.com/int/account/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className="material-icons">account_circle</i>
          </a>
          <Link
            className="button button-icon now-playing__logout"
            title="Log out"
            to="/logout"
          >
            <i className="material-icons">power_settings_new</i>
          </Link>
        </div>
      </div>
    );
  }
}

export default PageTitle;