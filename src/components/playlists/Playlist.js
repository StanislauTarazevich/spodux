import React, { PureComponent } from 'react';

class Playlist extends PureComponent {
  handleClick = () => {
    this.props.onSelectPlaylist(this.props.playlist.id);
  };

  render() {
    const { playlist }= this.props;

    return (
      <div className="playlists__item" onClick={this.handleClick}>
        <img className="playlists__item-image" src={playlist.image} alt={playlist.name} />
        <div className="playlists__item-name">{playlist.name}</div>
      </div>
    );
  }
}

export default Playlist