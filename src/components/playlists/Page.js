import React, { PureComponent } from 'react';
import PageTitle from '../PageTitle';
import Playlists from './Playlists';
import PlaylistsSummary from './Summary';
import PlaylistDetails from './PlaylistDetails';

class PlaylistsPage extends PureComponent {
  handleShowMore = () => {
    this.props.getMorePlaylists(this.props.next);
  };

  render() {
    const {
      playlists,
      total,
      query,
      next,
      currentPlaylist,
      currentPlaylistTracks,
      currentPlaylistTracksTotal,
      currentPlaylistTracksNext,
      onSearch,
      onSelectPlaylist,
      onModalClose,
      getMorePlaylistTracks,
      onListenPreview
    } = this.props;

    return (
      <div>
        <PageTitle>
          My Playlists
        </PageTitle>
        <PlaylistsSummary total={total} query={query} visibleCount={playlists.length} onSearch={onSearch} />
        <Playlists playlists={playlists} onSelectPlaylist={onSelectPlaylist} />
        <PlaylistDetails
          isOpen={Boolean(currentPlaylist)}
          playlist={currentPlaylist}
          tracks={currentPlaylistTracks}
          total={currentPlaylistTracksTotal}
          next={currentPlaylistTracksNext}
          onModalClose={onModalClose}
          getMorePlaylistTracks={getMorePlaylistTracks}
          onListenPreview={onListenPreview}
        />
        {next && <div className="text-center"><button className="button" onClick={this.handleShowMore}>Show more</button></div>}
      </div>
    );
  }
}

export default PlaylistsPage;