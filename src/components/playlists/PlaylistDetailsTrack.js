import React, { PureComponent } from 'react';
import classNames from 'classnames';

class PlaylistDetailsTrack extends PureComponent {
  hasPreview() {
    const { track } = this.props;
    return Boolean(track && track.preview);
  }

  handleListenPreview = () => {
    if (this.hasPreview()) {
      this.props.onListenPreview(this.props.track);
    }
  };

  render() {
    const { track } = this.props;

    const previewClass = classNames({
      'playlists__modal-track-control': true,
      'playlists__modal-track-control--disabled': !this.hasPreview()
    });

    return (
      <div className="playlists__modal-track">
        <div className="playlists__modal-track-image-container">
          <img src={track.albumImage} alt={track.name} className="full-width playlists__modal-track-image"/>
        </div>
        <div className="playlists__modal-track-name">
          {track.name}
          <br/>
          <small>{track.artistName}</small>
        </div>
        <div className="playlists__modal-track-controls">
          <span className={previewClass} onClick={this.handleListenPreview}>
            <i className="material-icons">audiotrack</i>
          </span>
        </div>
      </div>
    );
  }
}

export default PlaylistDetailsTrack;