import React, { PureComponent } from 'react';
import PlaylistDetailsTrack from './PlaylistDetailsTrack';

class PlaylistDetailsTracks extends PureComponent {
  handleShowMore = () => {
    this.props.getMorePlaylistTracks(this.props.next);
  };

  render() {
    const {
      tracks,
      next,
      onListenPreview
    } = this.props;

    if (!tracks) {
      return 'Loading...'
    } else if (!tracks.length) {
      return 'There are no tracks in this playlist'
    } else {
      return (
        <div className="playlists__modal-tracks-inner">
          {tracks.map((track) => (
            <PlaylistDetailsTrack
              key={track.id}
              track={track}
              onListenPreview={onListenPreview}
            />
          ))}
          {next && <div className="text-center"><button className="button" onClick={this.handleShowMore}>Show more</button></div>}
        </div>
      );
    }
  }
}

export default PlaylistDetailsTracks;