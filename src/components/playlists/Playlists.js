import React, { PureComponent } from 'react';
import Playlist from './Playlist';

class Playlists extends PureComponent {
  render() {
    const {
      playlists,
      onSelectPlaylist
    } = this.props;

    return (
      <div className="playlists__list">
        {playlists.map((playlist) => (
          <Playlist
            onSelectPlaylist={onSelectPlaylist}
            key={playlist.id}
            playlist={playlist}
          />
        ))}
      </div>
    );
  }
}

export default Playlists;