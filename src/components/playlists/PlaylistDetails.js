import React, { PureComponent } from 'react';
import ReactModal from 'react-modal';
import PlaylistDetailsTracks from './PlaylistDetailsTracks'

class PlaylistDetails extends PureComponent {
  state = {
    height: null
  };

  componentDidMount() {
    this.updateHeight();
  }

  componentDidUpdate() {
    this.updateHeight();
  }

  updateHeight() {
    if (this.summaryElement) {
      const summaryHeight = this.summaryElement.offsetHeight;
      this.setState({
        height: `calc(100% - ${summaryHeight}px)`
      });
    }
  }

  handleCloseModal = () => {
    this.props.onModalClose();
  };

  render() {
    const {
      isOpen,
      playlist,
      tracks,
      total,
      next,
      getMorePlaylistTracks,
      onListenPreview
    } = this.props;

    const height = this.state.height || 'auto';

    let content;
    if (playlist) {
      content =
        <div className="playlists__modal-inner">
          <div
            className="playlists__modal-summary"
            ref={(el) => { this.summaryElement = el; }}
          >
            <div className="playlists__modal-image">
              <img src={playlist.image} className="full-width" alt={playlist.name} />
            </div>
            <div className="playlists__modal-title">
              {playlist.name}
              <br/>
              {total && <small>{total} tracks</small>}
            </div>
            <div className="playlists__modal-close" onClick={this.handleCloseModal}>
              <i className="material-icons">clear</i>
            </div>
          </div>
          <div
            className="playlists__modal-tracks"
            style={{height}}
          >
            <PlaylistDetailsTracks
              tracks={tracks}
              next={next}
              getMorePlaylistTracks={getMorePlaylistTracks}
              onListenPreview={onListenPreview}
            />
          </div>
        </div>;
    } else {
      content = '';
    }

    return (
      <ReactModal
        isOpen={isOpen}
        className="playlists__modal"
        overlayClassName="modal-overlay"
        onRequestClose={this.handleCloseModal}
      >
        {content}
      </ReactModal>
    );
  }
}

export default PlaylistDetails