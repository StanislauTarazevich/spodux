import React, { PureComponent } from 'react';

class PlaylistSummary extends PureComponent {
  handleSearch = (event) => {
    this.props.onSearch(event.target.value);
  };

  render() {
    const {
      total,
      visibleCount,
      query
    } = this.props;

    return (
      <div className="playlists__summary text-center">
        <input
          className="playlists__search-input"
          type="text"
          value={query}
          onChange={this.handleSearch}
          placeholder="Search..."
        />
        <div>{visibleCount}/{total} playlists visible</div>
      </div>
    );
  }
}

export default PlaylistSummary;