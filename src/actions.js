import { createAction } from 'redux-actions';

export const setTokens = createAction('SET_TOKENS');
export const setInitialization = createAction('SET_INITIALIZATION');
export const removeTokens = createAction('REMOVE_TOKENS');

export const setSearchQuery = createAction('SET_SEARCH_QUERY');

export const setSearchResults = createAction('SET_SEARCH_RESULTS');
export const addSearchResults = createAction('ADD_SEARCH_RESULTS');
export const setCurrentPlayback = createAction('SET_CURRENT_PLAYBACK');

export const setPlaylists = createAction('SET_PLAYLISTS');
export const addPlaylists = createAction('ADD_PLAYLISTS');
export const setPlaylistsQuery = createAction('SET_PLAYLISTS_QUERY');

export const setCurrentPlaylist = createAction('SET_CURRENT_PLAYLIST');
export const removeCurrentPlaylist = createAction('REMOVE_CURRENT_PLAYLIST');
export const setCurrentPlaylistTracks = createAction('SET_CURRENT_PLAYLIST_TRACKS');
export const addCurrentPlaylistTracks = createAction('ADD_CURRENT_PLAYLIST_TRACKS');

export const setPlayer = createAction('SET_PLAYER');
export const resetPlayer = createAction('RESET_PLAYER');
